﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Core.Db;
using Core.Models;

using Microsoft.EntityFrameworkCore;

namespace API.Db
{
    /// <summary>
    /// This class handles database interaction for reviews.
    /// </summary>
    public class BookingDbHandler : IBookingDbHandler
    {
        private readonly DbContext context;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="context"></param>
        public BookingDbHandler(DbContext context)
        {
            this.context = context;
        }

        #region Async
        /// <inheritdoc/>
        public async Task<List<Booking>> GetBookingsAsync()
        {
            return await this.context.Bookings
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<Booking> GetBookingAsync(Guid id)
        {
            Booking booking = await this.context.Bookings
                .FirstAsync(booking => booking.BookingId == id);

            if (booking == null)
                throw new ArgumentException("No booking found with id " + id);

            return booking;
        }

        /// <inheritdoc/>
        public async Task CreateBookingAsync(Booking booking, bool saveChanges = true)
        {
            this.context.Bookings.Add(booking);
            if (saveChanges)
                await this.context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task UpdateBookingAsync(Booking booking, bool saveChanges = true)
        {
            try
            {
                if (!await this.context.Bookings.AnyAsync(bookingCheck => booking.BookingId == bookingCheck.BookingId))
                    throw new ArgumentException("No booking found with id " + booking.BookingId);

                this.context.Entry(booking).State = EntityState.Modified;

                if (saveChanges)
                    await this.context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        /// <inheritdoc/>
        public async Task DeleteBookingAsync(Guid id, bool saveChanges = true)
        {
            Booking booking = await this.context.Bookings.FindAsync(id);

            if (booking == null)
                throw new ArgumentException("No booking found with id " + id);

            this.context.Bookings.Remove(booking);

            if (saveChanges)
                await this.context.SaveChangesAsync();
        }
        #endregion

        #region Sync
        /// <inheritdoc/>
        public List<Booking> GetBookings()
        {
            return this.context.Bookings
                .ToList();
        }

        /// <inheritdoc/>
        public Booking GetBooking(Guid id)
        {
            Booking booking = this.context.Bookings
                .FirstOrDefault(booking => booking.BookingId == id);

            if (booking == null)
                throw new ArgumentException("No booking found with id " + id);

            return booking;
        }

        /// <inheritdoc/>
        public void CreateBooking(Booking booking, bool saveChanges = true)
        {
            this.context.Bookings.Add(booking);
            if (saveChanges)
                this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateBooking(Booking booking, bool saveChanges = true)
        {
            try
            {
                if (!this.context.Bookings.Any(bookingCheck => booking.BookingId == bookingCheck.BookingId))
                    throw new ArgumentException("No booking found with id " + booking.BookingId);

                this.context.Entry(booking).State = EntityState.Modified;

                if (saveChanges)
                    this.context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        /// <inheritdoc/>
        public void DeleteBooking(Guid id, bool saveChanges = true)
        {
            Booking booking = this.context.Bookings.Find(id);

            if (booking == null)
                throw new ArgumentException("No booking found with id " + id);

            this.context.Bookings.Remove(booking);

            if (saveChanges)
                this.context.SaveChanges();
        }
        #endregion
    }
}
