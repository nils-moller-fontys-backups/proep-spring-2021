﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

using API.Controllers;
using API.Db;
using API.Middleware;
using API.Wrappers;

using Core.Controllers;
using Core.Db;
using Core.Wrappers;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

using DbContext = API.Db.DbContext;

namespace API
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private IConfiguration Config { get; }

        public Startup(IConfiguration configuration)
        {
            this.Config = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());

                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "ProEP API",
                    Description = "The API for the ProEP project written in ASP.NET Core 3"
                });

                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "API.xml"));
            });

            services.AddSwaggerGenNewtonsoftSupport();

            // db contexts
            services.AddDbContextPool<DbContext>(options =>
            {
                options.UseMySql(
                    this.Config["DbConnectionString"],
                    mySqlOptions => mySqlOptions
                        .ServerVersion(new Version(5, 7, 26), ServerType.MySql)
                        .CharSetBehavior(CharSetBehavior.NeverAppend).EnableRetryOnFailure())
                        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            // controllers
            services.AddScoped<IJobDbHandler, JobDbHandler>();
            services.AddScoped<IBookingDbHandler, BookingDbHandler>();
            services.AddScoped<IUserDbHandler, UserDbHandler>();
            services.AddScoped<IReviewDbHandler, ReviewDbHandler>();

            services.AddScoped<IJobsController, JobsController>();
            services.AddScoped<IBookingsController, BookingsController>();
            services.AddScoped<IReviewsController, ReviewsController>();
            services.AddScoped<IUsersController, UsersController>();

            // wrappers
            services.AddScoped<IValidateAsyncWrapper, ValidateAsyncWrapper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProEP API v1");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                );

            app.UseMiddleware<GoogleJwtMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
