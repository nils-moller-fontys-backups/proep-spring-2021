using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Attributes;

using Core.Controllers;
using Core.Db;
using Core.Models;

using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// This controller handles the interaction with bookings.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class BookingsController : ControllerBase, IBookingsController
    {
        private IBookingDbHandler BookingDbHandler { get; }
        private IJobDbHandler JobDbHandler { get; }
        private IUserDbHandler UserDbHandler { get; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="bookingDbHandler">The database context for bookings.</param>
        /// <param name="jobDbHandler">The database context for jobs.</param>
        /// <param name="userDbHandler">The database context for users.</param>
        public BookingsController(IBookingDbHandler bookingDbHandler, IJobDbHandler jobDbHandler, IUserDbHandler userDbHandler)
        {
            this.BookingDbHandler = bookingDbHandler;
            this.JobDbHandler = jobDbHandler;
            this.UserDbHandler = userDbHandler;
        }

        // GET: Bookings
        /// <summary>
        /// Gets all the bookings.
        /// </summary>
        /// <returns>
        /// Ok() with the collection of all bookings.
        /// </returns>
        [HttpGet]
        public async Task<ActionResult<List<Booking>>> GetBookings()
        {
            return this.Ok(await this.BookingDbHandler.GetBookingsAsync());
        }

        // GET: Bookings/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get a booking.
        /// </summary>
        /// <param name="id">The id of the booking to look for.</param>
        /// <returns>
        /// Ok() with the booking with matching id. <br></br>
        /// NotFound() if no booking was found. <br></br>
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Booking>> GetBooking(Guid id)
        {
            try
            {
                return this.Ok(await this.BookingDbHandler.GetBookingAsync(id));
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // GET: Bookings/job/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get the bookings for a specific job.
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns>
        /// Ok() with the list of bookings with matching job id. <br></br>
        /// BadRequest() if the job does not exist. <br></br>
        /// </returns>
        [HttpGet("job/{jobId}")]
        public async Task<ActionResult<List<Booking>>> GetBookingsForJob(Guid jobId)
        {
            try
            {
                await this.JobDbHandler.GetJobAsync(jobId); // if the job doesnt exist
                return this.Ok((await this.BookingDbHandler.GetBookingsAsync()).Where(booking => booking.JobId == jobId));
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        // POST: Bookings
        /// <summary>
        /// Creates a new booking.
        /// </summary>
        /// <param name="booking">The booking to create.</param>
        /// <returns>
        /// Unauthorized() if the logged in user is not the consumer of the booking. <br></br>
        /// BadRequest() if the consumer does not exist. <br></br>
        /// BadRequest() if the job does not exist. <br></br>
        /// BadRequest() if the consumer is the provider. <br></br>
        /// BadRequest() if the job already has a booking for the given date. <br></br>
        /// CreatedAtAction() with the URI to the booking created and the booking itself. <br></br>
        /// </returns>
        /// <remarks>
        /// Required fields:<br></br>
        /// JobId<br></br>
        /// ConsumerId<br></br>
        /// Address<br></br>
        /// City<br></br>
        /// BookingDate<br></br>
        /// </remarks>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Booking>> CreateBooking(Booking booking)
        {
            try
            {
                if (((User)this.HttpContext.Items["User"]).UserId != booking.ConsumerId)
                    return this.Unauthorized();

                await this.UserDbHandler.GetUserAsync(booking.ConsumerId); // if consumer doesnt exist
                await this.JobDbHandler.GetJobAsync(booking.JobId); // if job doesnt exist
                if ((await this.JobDbHandler.GetJobAsync(booking.JobId)).ProviderId == booking.ConsumerId)
                    return this.BadRequest("User cannot hire himself.");

                if ((await this.BookingDbHandler.GetBookingsAsync())
                        .Where(bookingCheck => bookingCheck.JobId == booking.JobId)
                        .FirstOrDefault(bookingCheck => bookingCheck.BookingDate.Date == booking.BookingDate.Date) != null)
                {
                    return this.BadRequest("Can only have one booking per job per day.");
                }

                booking.BookingId = Guid.NewGuid();

                DateTime timeCreated = DateTime.Now;
                booking.DateCreated = timeCreated;
                booking.LastUpdated = timeCreated;

                booking.Status = Core.DataTypes.STATUS.Pending;

                await this.BookingDbHandler.CreateBookingAsync(booking);
                return this.CreatedAtAction(nameof(GetBooking), new { id = booking.BookingId }, booking);
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        // PUT: Bookings/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Update a booking.
        /// </summary>
        /// <param name="id">The id of the booking to update.</param>
        /// <param name="booking">The booking to update.</param>
        /// <returns>
        /// Unauthorized() if the logged in user is not the consumer.<br></br>
        /// BadRequest() if the id does not match the booking id.<br></br>
        /// BadRequest() if illegal fields are being updated. <br></br>
        /// NoContent() if the update was successful. <br></br>
        /// NotFound() if the booking does not exist. <br></br>
        /// </returns>
        /// <remarks>
        /// Fields available for update:
        /// Address
        /// City
        /// Status
        /// BookingDate
        /// </remarks>
        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult> UpdateBooking(Guid id, Booking booking)
        {
            try
            {
                if (((User)this.HttpContext.Items["User"]).UserId != booking.ConsumerId)
                    return this.Unauthorized();

                if (id != booking.BookingId)
                    return this.BadRequest("Ids do not match");

                Booking existingBooking = await this.BookingDbHandler.GetBookingAsync(id);
                if (existingBooking.BookingId != booking.BookingId || existingBooking.JobId != booking.JobId || existingBooking.ConsumerId != booking.ConsumerId || existingBooking.DateCreated != booking.DateCreated)
                    return this.BadRequest("Cannot update id, job id, consumer id or date created.");

                booking.LastUpdated = DateTime.Now;

                await this.BookingDbHandler.UpdateBookingAsync(booking);

                return this.NoContent();
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // DELETE: Bookings/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Deletes a booking.
        /// </summary>
        /// <param name="id">The id of the booking to delete.</param>
        /// <returns>
        /// NotFound() if the id does not exist. <br></br>
        /// Unauthorized() if the user is not the consumer. <br></br>
        /// Ok() once the booking is deleted. <br></br>
        /// </returns>
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> DeleteBooking(Guid id)
        {
            try
            {
                Booking booking = await this.BookingDbHandler.GetBookingAsync(id);

                if (((User)this.HttpContext.Items["User"]).UserId != booking.ConsumerId)
                    return this.Unauthorized();

                await this.BookingDbHandler.DeleteBookingAsync(booking.BookingId);
                return this.Ok();
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }
    }
}
