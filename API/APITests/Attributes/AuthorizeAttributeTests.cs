﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using Moq;

using Xunit;

namespace API.Attributes.Tests
{
    public class AuthorizeAttributeTests : IDisposable
    {

        // setup
        public AuthorizeAttributeTests()
        {
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task OnAuthorization_SetsContextResultToUnauthorized_WhenUserDoesNotExist()
        {
            // Arrange
            Mock<HttpContext> httpContextMock = new Mock<HttpContext>();
            httpContextMock
                .SetupGet(context => context.Items)
                .Returns(new Dictionary<object, object>());
            ActionContext actionContextMock = new ActionContext(httpContextMock.Object,
                new Microsoft.AspNetCore.Routing.RouteData(),
                new Microsoft.AspNetCore.Mvc.Abstractions.ActionDescriptor());
            AuthorizationFilterContext authFilterContextMock = new AuthorizationFilterContext(actionContextMock, new List<IFilterMetadata>());

            // Act
            new AuthorizeAttribute().OnAuthorization(authFilterContextMock);
            JsonResult result = (JsonResult)authFilterContextMock.Result;

            // Assert
            Assert.Equal(StatusCodes.Status401Unauthorized, result.StatusCode);
            Assert.Equal("Unauthorized", result.Value);
        }
    }
}