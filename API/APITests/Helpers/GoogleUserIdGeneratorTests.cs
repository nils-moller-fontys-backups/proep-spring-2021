﻿using System;

using Xunit;

namespace API.Helpers.Tests
{
    public class GoogleUserIdGeneratorTests : IDisposable
    {
        public GoogleUserIdGeneratorTests()
        {

        }

        public void Dispose()
        {

        }

        [Fact]
        public void GenerateRandomGoogleUserIdTest()
        {
            // Act
            string generatedId = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            // Assert
            Assert.NotNull(generatedId);
            Assert.Equal(21, generatedId.Length);
        }
    }
}