﻿using System;
using System.Threading.Tasks;

using Xunit;

namespace Core.Models.Tests
{
    public class JobTests : IDisposable
    {
        private readonly Job job;

        // setup
        public JobTests()
        {
            this.job = new Job { JobId = Guid.Empty };
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task Job_EqualsObject_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            object jobCheck = new Job { JobId = Guid.Empty };

            // Act
            bool result = this.job.Equals(jobCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Job_EqualsObject_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            object jobCheck = new Job { JobId = Guid.NewGuid() };

            // Act
            bool result = this.job.Equals(jobCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Job_EqualsJob_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.Empty };

            // Act
            bool result = this.job.Equals(jobCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Job_EqualsJob_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.NewGuid() };

            // Act
            bool result = this.job.Equals(jobCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Job_EqualsOperator_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.Empty };

            // Act
            bool result = this.job == jobCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Job_EqualsOperator_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.NewGuid() };

            // Act
            bool result = this.job == jobCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Job_NotEqualsOperator_ReturnsTrueWhenIdsDoNotMatch()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.NewGuid() };

            // Act
            bool result = this.job != jobCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Job_NotEqualsOperator_ReturnsFalseWhenIdsMatch()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.Empty };

            // Act
            bool result = this.job != jobCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Job_GetHashCode_IsEqualWhenIdIsEqual()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.Empty };

            // Act
            int jobCode = this.job.GetHashCode();
            int jobCheckCode = jobCheck.GetHashCode();

            // Assert
            Assert.Equal(jobCheckCode, jobCode);
        }

        [Fact]
        public async Task Job_GetHashCode_IsDifferentWhenIdIsDifferent()
        {
            // Arrange
            Job jobCheck = new Job { JobId = Guid.NewGuid() };

            // Act
            int jobCode = this.job.GetHashCode();
            int jobCheckCode = jobCheck.GetHashCode();

            // Assert
            Assert.NotEqual(jobCheckCode, jobCode);
        }
    }
}