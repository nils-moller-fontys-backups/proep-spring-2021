﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

namespace Core.Db
{
    /// <summary>
    /// This interface defines the methods for the review context.
    /// </summary>
    public interface IReviewDbHandler
    {
        #region Async
        /// <summary>
        /// Get all reviews.
        /// </summary>
        /// <returns>The collection of reviews currently stored.</returns>
        Task<List<Review>> GetReviewsAsync();

        /// <summary>
        /// Get a review.
        /// </summary>
        /// <param name="reviewId">The id of the review to delete.</param>
        /// <returns>The review with the given ids.</returns>
        /// <exception cref="ArgumentException">Throws when no review with the id could be found.</exception>
        Task<Review> GetReviewAsync(Guid reviewId);

        /// <summary>
        /// Create a new review.
        /// </summary>
        /// <param name="review">The review object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        Task CreateReviewAsync(Review review, bool saveChanges = true);

        /// <summary>
        /// Delete a review.
        /// </summary>
        /// <param name="reviewId">The id of the review to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no review with the id could be found.</exception>
        Task DeleteReviewAsync(Guid reviewId, bool saveChanges = true);
        #endregion

        #region Sync
        /// <summary>
        /// Get all reviews.
        /// </summary>
        /// <returns>The collection of reviews currently stored.</returns>
        List<Review> GetReviews();

        /// <summary>
        /// Get a review.
        /// </summary>
        /// <param name="reviewId">The id of the review to delete.</param>
        /// <returns>The review with the given ids.</returns>
        /// <exception cref="ArgumentException">Throws when no review with the id could be found.</exception>
        Review GetReview(Guid reviewId);

        /// <summary>
        /// Create a new review.
        /// </summary>
        /// <param name="review">The review object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        void CreateReview(Review review, bool saveChanges = true);

        /// <summary>
        /// Delete a review.
        /// </summary>
        /// <param name="reviewId">The id of the review to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no review with the id could be found.</exception>
        void DeleteReview(Guid reviewId, bool saveChanges = true);
        #endregion
    }
}
