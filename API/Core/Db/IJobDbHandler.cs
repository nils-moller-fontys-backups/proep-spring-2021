﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

namespace Core.Db
{
    /// <summary>
    /// This interface defines the methods for the job context.
    /// </summary>
    public interface IJobDbHandler
    {
        #region Async
        /// <summary>
        /// Get all jobs.
        /// </summary>
        /// <returns>The collection of jobs currently stored.</returns>
        Task<List<Job>> GetJobsAsync();

        /// <summary>
        /// Get a job.
        /// </summary>
        /// <param name="id">The id of the job to get.</param>
        /// <returns>The job with the given id.</returns>
        /// <exception cref="ArgumentException">Throws when no job with the id could be found.</exception>
        Task<Job> GetJobAsync(Guid id);

        /// <summary>
        /// Create a new job.
        /// </summary>
        /// <param name="job">The job object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        Task CreateJobAsync(Job job, bool saveChanges = true);

        /// <summary>
        /// Update a job.
        /// </summary>
        /// <param name="job">The job with updated values.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no job with the id could be found.</exception>
        Task UpdateJobAsync(Job job, bool saveChanges = true);

        /// <summary>
        /// Delete a job.
        /// </summary>
        /// <param name="id">The id of the job to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no job with the id could be found.</exception>
        Task DeleteJobAsync(Guid id, bool saveChanges = true);
        #endregion

        #region Sync
        /// <summary>
        /// Get all jobs.
        /// </summary>
        /// <returns>The collection of jobs currently stored.</returns>
        List<Job> GetJobs();

        /// <summary>
        /// Get a job.
        /// </summary>
        /// <param name="id">The id of the job to get.</param>
        /// <returns>The job with the given id.</returns>
        /// <exception cref="ArgumentException">Throws when no job with the id could be found.</exception>
        Job GetJob(Guid id);

        /// <summary>
        /// Create a new job.
        /// </summary>
        /// <param name="job">The job object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        void CreateJob(Job job, bool saveChanges = true);

        /// <summary>
        /// Update a job.
        /// </summary>
        /// <param name="job">The job with updated values.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no job with the id could be found.</exception>
        void UpdateJob(Job job, bool saveChanges = true);

        /// <summary>
        /// Delete a job.
        /// </summary>
        /// <param name="id">The id of the job to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no job with the id could be found.</exception>
        void DeleteJob(Guid id, bool saveChanges = true);
        #endregion
    }
}
