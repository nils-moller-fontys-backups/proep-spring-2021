﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers
{
    /// <summary>
    /// This interface defines structure of the interaction with reviews.
    /// </summary>
    public interface IReviewsController
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        Task<ActionResult<List<Review>>> GetReviews();
        Task<ActionResult<Review>> GetReviewById(Guid reviewId);
        Task<ActionResult<List<Review>>> GetReviewsByReviewer(string reviewerId);
        Task<ActionResult<List<Review>>> GetReviewsByReviewee(string revieweeId);
        Task<ActionResult<List<Review>>> GetReviewsByReviewerAndReviewee(string reviewerId, string revieweeId);
        Task<ActionResult<Review>> CreateReview(Review review);
        Task<ActionResult> DeleteReview(Guid reviewId);
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
