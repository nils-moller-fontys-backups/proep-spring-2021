﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers
{
    /// <summary>
    /// This interface defines structure of the interaction with users.
    /// </summary>
    public interface IUsersController
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        Task<ActionResult<List<User>>> GetUsers();
        Task<ActionResult<User>> GetUser(string id);
        Task<ActionResult> DeleteUser(string id);
        Task<ActionResult> Register(User user);
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
