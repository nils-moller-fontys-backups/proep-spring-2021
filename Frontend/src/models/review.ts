import { User } from "./user"

export class Review {

      public reviewId!: string;
      public reviewerId!: string;
      public revieweeId!: string;
      public title!: string;
      public description!: string;
      public rating!: number;
      public pros!: string;
      public cons!: string;
      public dateCreated!: string;
    
  }