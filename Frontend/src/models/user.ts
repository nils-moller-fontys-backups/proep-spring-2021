import { Job } from "./job"
import { Booking } from "./booking"

export class User {
      public userId!: string;
      public email!: string;
      public firstName!: string;
      public lastName!: string;
      public dateOfBirth!: Date;
      public profilePictureLink!: string;
      public phone!: string;
      public address!: string;
      public city!: string;
      public rating!: number;
      public dateCreated!: string;
      public lastLogin!: string;
      public token!: string;
    
  }