import { User } from "./user"
import { Booking } from './booking';
export class Job {
   
      public jobId!: string;
      public providerId!: string;
      public price!: number;
      public title!: string;
      public description!: string;
      public levelOfExperience!: number;
      public dateCreated!: Date;
      public lastUpdated!: Date;
 
 
  }

