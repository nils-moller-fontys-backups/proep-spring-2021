import { Component, ViewEncapsulation, OnInit, ViewChild, ElementRef, ErrorHandler } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetApiService } from '../get-api.service';
import { Job } from '../../models/job';
import { Booking } from 'src/models/booking';
import { User } from 'src/models/user';
import { MatCalendarCellClassFunction } from '@angular/material/datepicker';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class JobComponent implements OnInit {

  mindate!: Date;
  bookingdate!: Date;
  selectedJob!: Job;
  selectedUser!: User;
  UserProfile!: User;
  bookings!: Booking[];
  id!: string;
  dataloaded = false;
  hasBookings = false;
  lasterror: any;
  private busyDates!: Date[];


  constructor(private route: ActivatedRoute, public api: GetApiService,  private modalService: NgbModal){}
  
  
  closeResults!: string;
  reactiveForm!: FormGroup;
  @ViewChild('error') error!: ElementRef;
  @ViewChild('success') success!: ElementRef;
  @ViewChild('confirmDeletion') confirmDeletion!: ElementRef;
  @ViewChild('successDelete') successDelete!: ElementRef;


  ngOnInit(): void {
    
    this.mindate = new Date();
    this.busyDates = [];
    this.route.paramMap
    .subscribe(params => {
      console.log(params);
      var jobId = params.get('id');
      console.log(jobId);
      this.id = jobId!;
      this.api.getJob(jobId!).subscribe((job)=>{
        this.selectedJob = job;
        console.log(this.selectedJob);
        this.getBookings();
        this.getUser();
        this.getCurrentUser();
    })
      
     // params.get('blabla');
    })

  }

async getBookings(){
  // await this.delay(250);
  
  this.api.getBookingsByJobID(this.selectedJob.jobId).subscribe((bookings) =>{
    this.bookings = bookings;
    console.log(this.bookings);
    this.hasBookings = true;
  })    
      await this.delay(350);
      this.dataloaded = true;
      this.bookings.forEach(booking => {
        console.log(booking.bookingDate);
        var tempdate = new Date(booking.bookingDate);
        this.busyDates.push(tempdate);
        // this.busyDates.push(booking.bookingDate);
      });
      this._setupClassFunction();
      console.log("Busy dates:");
      console.log(this.busyDates);


  }

async getUser(){
  this.api.getUser(this.selectedJob.providerId).subscribe((user)=>{
    this.selectedUser = user;
    console.log(this.selectedUser);
  })
}
async getCurrentUser(){
  await this.delay(1000);
  this.api.getUser(this.api.user.id).subscribe((user)=>{
    this.UserProfile = user;
    console.log("Current User from DB");
    console.log(this.UserProfile);
  })
}

getUserNamebyBooking(bok: Booking){
  let id = bok.consumerId;
  this.api.getUser(id).subscribe((customer: User)=>{
    let fullname = customer.firstName + " " + customer.lastName;
    return fullname;
  });
}

todeletejob(){
  this.open(this.confirmDeletion);
}

deletejob(){
  this.api.deleteJob(this.id).subscribe({
    next: data => {
        console.log('Delete successful');
        this.open(this.successDelete);
    },
    error: error => {
      this.lasterror = error.error;
        this.open(this.error);
        console.error('There was an error!', error);
    }
});
  console.log(this.id);
}
  dateClass!: (d: Date) => any;

private _setupClassFunction() {
  this.dateClass = (d: Date) => {
    let selected = false;

    if (this.busyDates) {
      selected = this.busyDates.some((item: Date) => 
          item.getFullYear() === d.getFullYear() 
          && item.getDate() === d.getDate() 
          && item.getMonth() === d.getMonth());
        
    }
    console.log(selected);
    return selected ? 'example-custom-date-class' : undefined;
  }
}

postBooking(){
  if(this.bookingdate){
    var booking = new Booking();
    booking.jobId = this.selectedJob.jobId;
    booking.consumerId = this.api.user.id;
    booking.address = this.UserProfile.address;
    booking.city = this.UserProfile.city;
    booking.bookingDate = this.bookingdate.toDateString();
  
    this.api.postBooking(booking).subscribe({
      next: data => {
          console.log(data);
          this.open(this.success);
         
      },
      error: error => {
        this.lasterror = error.error;
        this.open(this.error);
        console.error('There was an error!', error);
      }
  });
  }
  else{
    this.lasterror = "Please select the date.";
        this.open(this.error);
  }
  
}
// dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {
//   // Only highligh dates inside the month view.
//   if (view === 'month') {
//     let selected = false;
//     if (this.hasBookings){

//       selected = this.busyDates.some((item: Date)=>
//         item.getFullYear() === cellDate.getFullYear()
//         && item.getDate() === cellDate.getDate()
//         && item.getMonth() === cellDate.getMonth());

//       // Highlight the 1st and 20th day of each month.
//       return selected ? 'example-custom-date-class' : '';
//     }
    
//   }
// }

showdateinconsole(){
  console.log(this.bookingdate);
}
private delay(ms: number)
{
  return new Promise(resolve => setTimeout(resolve, ms));
}

open(content: any) {
  
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', backdrop : 'static',
  keyboard : false}).result.then((result) => {
    this.closeResults = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResults = `Dismissed ${this.getDismissReason(reason)}`;
   
  });
}

close(string: String){
  if (string=="success"){
    window.location.reload();
  }
  else{
    console.log("SAVED");
  }
  
 
}
private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
}


}





