import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocialAuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider} from 'angularx-social-login';
import { GetApiService } from '../get-api.service';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { User } from 'src/models/user';
import {faUserAlt , faIgloo, faSignOutAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {

  @ViewChild('content') content!: ElementRef;

  birthday: any;
  address: any;
  phonenr: any;
  city: any;
  closeResults!: string;
  reactiveForm!: FormGroup;
  isSignedin!: boolean;
  faUserAlt = faUserAlt;
  faIgloo = faIgloo;
  faSignOutAlt = faSignOutAlt;
  constructor(private fb: FormBuilder, private socialAuthService: SocialAuthService, public api: GetApiService, private modalService: NgbModal) { }

  ngOnInit(): void {


    this.socialAuthService.authState.subscribe((user) => {
      this.api.user = user;
      this.isSignedin = (user != null);
      console.log(this.api.user);
    });
  }


  googleSignin(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(x =>
      {
        console.log(x);
        this.api.getUser(x.id).subscribe({
          next: user => {
              console.log('User Found');
              console.log(user);
          },
          error: error => {
              console.error('There was an error!', error);
              this.modalService.open(this.content);
          }
      });
      });

  }

  logout(): void {
    this.socialAuthService.signOut();
  }
  refreshToken(): void {
    this.socialAuthService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }

  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', backdrop : 'static',
    keyboard : false}).result.then((result) => {
      this.closeResults = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResults = `Dismissed ${this.getDismissReason(reason)}`;


    });
  }

  close(){
    console.log("SAVED");
    let newUser = new User();
    newUser.email = this.api.user.email;
    newUser.firstName = this.api.user.firstName;
    newUser.lastName = this.api.user.lastName;
    newUser.dateOfBirth = this.birthday
    newUser.profilePictureLink = this.api.user.photoUrl;
    newUser.phone = this.phonenr;
    newUser.address = this.address;
    newUser.city = this.city;
    newUser.userId = this.api.user.id;
    console.log("DATE:");
    console.log(this.birthday);
    console.log("User created?");
    console.log(newUser);
    this.api.postUserRegister(newUser, this.api.user.idToken).subscribe({
      next: data => {
          console.log(data);
      },
      error: error => {
          console.error('There was an error!', error);
      }
  });


  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
