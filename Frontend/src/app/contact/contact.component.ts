import { Component, OnInit } from '@angular/core';
import { faTelegramPlane} from '@fortawesome/free-brands-svg-icons';
import { faPhone, faEnvelopeOpenText, faHome} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  name!: string;
  email!: string;
  message!: string;
  faTelegramPlane = faTelegramPlane;
  faPhone = faPhone;
  faEnvelopeOpenText = faEnvelopeOpenText;
  faHome = faHome;
  constructor() { }

  ngOnInit(): void {
  }
  submitForm(){

  }
}
