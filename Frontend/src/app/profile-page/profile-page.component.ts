import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Job } from 'src/models/job';
import { User } from 'src/models/user';
import { Review } from 'src/models/review';
import { GetApiService } from '../get-api.service';
import { ActivatedRoute } from '@angular/router';
import { Booking } from 'src/models/booking';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {faHistory, faStar, faUser, faHome, faPhone, faPassport, faComments} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  UserProfile!: User;
  userJobs: Job[] = [];
  userReviews: Review[] = [];
  userAllBookings: Booking[] = [];
  gotJobs = false;
  gotReviews = false;
  UserId = "";
  bookingcounter = 0;
  leftareview = false;
  canleavereview = false;
  lasterror = "";
  userAge = 0;
  todayDate!: Date;

  reviewTitle: any;
  reviewScore: any;
  reviewFeedback: any;
  reviewPros: any;
  reviewCons: any;
  @ViewChild('leavereview') content!: ElementRef;
  @ViewChild('success') success!: ElementRef;
  @ViewChild('error') error!: ElementRef;

  faHistory = faHistory;
  faStar = faStar;
  faUser = faUser;
  faHome = faHome;
  faPhone = faPhone;
  faPassport = faPassport;
  faReview = faComments;


  constructor(private route: ActivatedRoute, public api: GetApiService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.gotJobs = false;
    this.gotReviews = false;
    this.route.paramMap
    .subscribe(params => {
      console.log("Params:");
      console.log(params);
      var UserId = params.get('id');
      console.log(UserId);
      this.api.getUser(UserId!).subscribe((user)=>{
        this.UserProfile = user;

        // Calculating user age
        let birthday = new Date(this.UserProfile.dateOfBirth);
        this.todayDate = new Date();
        console.log(this.todayDate);
        console.log(birthday);
        let timeDiff = Math.abs(this.todayDate.getTime() - birthday.getTime());
        console.log(timeDiff);
        let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
        console.log(age)
        this.userAge = age;


        console.log(this.UserProfile);
        if(!this.userJobs.length){
          this.getUserJobs(UserId!);
        }
    });



     // params.get('blabla');
    });

  }


  isShowPastJobs = false;
  isShowReviews = false;
  isShowExperience = false;
  isShowEducation = false;
  isShowVerifications = false;

  togglePastJobs() {
    if(this.isShowPastJobs == true){
      this.isShowPastJobs = false;
    }
    else{
    this.isShowPastJobs = true;
    this.isShowReviews = false;
    this.isShowExperience = false;
    this.isShowEducation = false;
    this.isShowVerifications = false;

    // if(!this.userJobs.length){
    //   this.getUserJobs(this.UserProfile.userId);
    // }
    }


  }

  toggleReviews() {

    if(this.isShowReviews == true){
      this.isShowReviews = false;
    }
    else{
      this.isShowPastJobs = false;
      this.isShowReviews = true;
      this.isShowExperience = false;
      this.isShowEducation = false;
      this.isShowVerifications = false;

      if(!this.userReviews.length){
        this.getUserReviews();
        this.canLeaveReview();
      }

    }


  }

  toggleExperience() {
    if(this.isShowExperience == true){
      this.isShowExperience = false;
    }
    else{
      this.isShowPastJobs = false;
      this.isShowReviews = false;
      this.isShowExperience = true;
      this.isShowEducation = false;
      this.isShowVerifications = false;
    }

  }

  toggleEducation() {

    if(this.isShowEducation == true){
      this.isShowEducation = false;
    }
    else{
    this.isShowPastJobs = false;
    this.isShowReviews = false;
    this.isShowExperience = false;
    this.isShowEducation = true;
    this.isShowVerifications = false;
    }

  }

  toggleVerifications() {

    if(this.isShowVerifications == true){
      this.isShowVerifications = false;
    }

    else{
    this.isShowPastJobs = false;
    this.isShowReviews = false;
    this.isShowExperience = false;
    this.isShowEducation = false;
    this.isShowVerifications = true;
    }

  }



  // API CALLS

  getUserReviews(){
    this.api.getReviewAsReviewee(this.UserProfile.userId).subscribe((reviews)=>{
      this.userReviews = reviews;
      console.log(this.userReviews);
      if(this.userReviews[0]){
        this.gotReviews = true;
      }

    })
  }
  getUserJobs(id: string){
    this.api.getJobFromUser(id).subscribe(async (jobs)=>{
      this.userJobs = jobs;
      console.log(this.userJobs);
      this.gotJobs = true;
    });

  }
  submitReview(){
    console.log("Submit Review method has been called");
    var completeReview = new Review();
    completeReview.reviewerId = this.api.user.id;
    completeReview.revieweeId = this.UserProfile.userId;
    completeReview.title = this.reviewTitle;
    completeReview.description = this.reviewFeedback;
    completeReview.rating = this.reviewScore;
    completeReview.pros = this.reviewPros;
    completeReview.cons = this.reviewCons;

    this.api.postReview(completeReview).subscribe({
      next: data => {
          console.log(data);
          window.location.reload();
      },
      error: error => {
          console.error('There was an error!', error);
      }
  });

  }

  async canLeaveReview()
  {
    this.canleavereview = false;
    this.api.getReviewsForRevieweeAsReviewer(this.UserProfile.userId, this.api.user.id).subscribe({
      next: data => {
          if(data.length){
            console.log("DATATATATAA");
            this.leftareview = true;
          };
        },
        error: error => {
           console.log("Error", error);
        }
      });
    await this.delay(150);
    if (this.leftareview == false)
    {
      this.userAllBookings = [];
    this.bookingcounter = 0;
    let dateTime = new Date();
    console.log("dateToday: " + dateTime);
    if (this.userJobs.length){
      this.userJobs.forEach(job => {
       this.api.getBookingsByJobID(job.jobId).subscribe({
        next: data => {
            console.log(data);
            data.forEach((booking: Booking) => {
              this.userAllBookings.push(booking);
              var tempdate = new Date(booking.bookingDate);
              console.log("tempdate: " + tempdate);

                if (tempdate < dateTime  && booking.consumerId == this.api.user.id ){
                  this.bookingcounter++;
                  console.log("FOUND DATE");
                }
              });
          },
          error: error => {
             console.log("Error", error);
          }
        });
      });
      await this.delay(250);
      /* If user booked something from this person in the past */
      if (this.bookingcounter >= 1){
        this.canleavereview = true;
      console.log("User booked this worker for " +  this.bookingcounter + " times in the past");
      }
    }
    }
  }

private delay(ms: number)
{
  return new Promise(resolve => setTimeout(resolve, ms));
}

open(content: any) {
  this.reviewTitle = "";
  this.reviewFeedback = "";
  this.reviewPros = "";
  this.reviewCons = "";
  this.reviewScore = 0;
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', backdrop : 'static',
  keyboard : false});
}

close(){
  console.log("Closed");
}

sendReview()
{
  console.log("SAVED");
  console.log("Submit Review method has been called");
  var completeReview = new Review();
  completeReview.reviewerId = this.api.user.id;
  completeReview.revieweeId = this.UserProfile.userId;
  completeReview.title = this.reviewTitle;
  completeReview.description = this.reviewFeedback;
  completeReview.rating = this.reviewScore;
  completeReview.pros = this.reviewPros;
  completeReview.cons = this.reviewCons;

  this.api.postReview(completeReview).subscribe({
    next: data => {
      console.log(data);
      this.open(this.success);
    },
    error: error => {
      if(error.error.title)
      {
        this.lasterror = error.error.title;
      }
      else{
        this.lasterror = error.error;
      }

        this.open(this.error);
        console.error('There was an error!', error);
    }
})

}

}
