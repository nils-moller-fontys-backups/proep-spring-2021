import { Component } from '@angular/core';
import { GetApiService } from './get-api.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Frontend';
  constructor( private api:GetApiService)
  {

  }
  ngOnInit()
  {
    this.api.pingCall().subscribe((data)=>{
      console.warn("get api data:", data);
    })
    // this.api.getJobs().subscribe((data)=>{
      
    // })
  }
}
