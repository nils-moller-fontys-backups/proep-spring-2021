import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Job } from 'src/models/job';
import { GetApiService } from '../get-api.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  jobtitle: any;
  jobprice: any;
  jobdescription: any;
  jobexperience: any;
  lasterror: any;

  @ViewChild('content') content!: ElementRef;
  @ViewChild('error') error!: ElementRef;
  @ViewChild('success') success!: ElementRef;
  queryField: FormControl = new FormControl();

  allJobs: Job[] = [];
  searchJob!: string;

  closeResults!: string;
  reactiveForm!: FormGroup;

  constructor(public api: GetApiService,  private modalService: NgbModal) { }

  ngOnInit(): void {
    this.queryField.valueChanges.subscribe(result=> console.log(result));
    this.getJobs();
  }

  async getJobs(){
    this.api.getJobs().subscribe((jobs)=>{
      this.allJobs = jobs;
      console.log(this.allJobs);
  });
  }

  displayFn(job: Job): string {
    return job!.title;
  }
  

// MODAL

open(content: any) {
  this.jobtitle = "";
  this.jobprice = "";
  this.jobdescription = "";
  this.jobexperience = "";
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', backdrop : 'static',
  keyboard : false}).result.then((result) => {
    this.closeResults = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResults = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

close(){
  console.log("SAVED");
  let newJob: Job = new Job;
  newJob.title = this.jobtitle;
  newJob.price = this.jobprice;
  newJob.description = this.jobdescription;
  newJob.levelOfExperience = this.jobexperience;
  newJob.providerId = this.api.user.id;
  let dateTime = new Date();
  newJob.dateCreated = dateTime;
  console.log(newJob);
  this.addJob(newJob);
  this.jobtitle = "";
  this.jobtitle = "";
  this.jobdescription = "";
  this.jobexperience = "";
}
closepop(){
  
}
  

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
}

addJob(job:Job){
    
  let errorMessage: any;
  this.api.postJob(job, this.api.user.idToken).subscribe({
    next: data => {
      console.log(data);
      this.open(this.success);
    },
    error: error => {
        this.lasterror = error.error.title;
        this.open(this.error);
        console.error('There was an error!', error);
    }
})
}
private delay(ms: number)
{
  return new Promise(resolve => setTimeout(resolve, ms));
}

}
