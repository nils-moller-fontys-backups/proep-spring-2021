# ProEP Spring 2021
This repository contains both the frontend and backend of the ProEP project from spring 2021.

## Database
We are using a MySQL database to store data. It is hosted on heroku.

## API
The API is written in ASP .NET Core 3.1.
It is using an ORM called Entity Framework Core to interact the databse.

## Website/Mobile app
The frontend is a PWA (progressive web app). This means it can run as a website and on mobile devices as a native app.